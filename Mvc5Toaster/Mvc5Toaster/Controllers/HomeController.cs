﻿using Mvc5Toaster.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc5Toaster.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

           this.AddToastMessage("My first message", "A lot of important text", ToastType.Info);
           this.AddToastMessage("My first message", "A lot of important text", ToastType.Success);
           this.AddToastMessage("My first message", "A lot of important text", ToastType.Error);
           this.AddToastMessage("My first message", "A lot of important text", ToastType.Warning);
            return View();
           

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
